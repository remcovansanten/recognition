import glob
import multiprocessing as mp
import os
from timeit import default_timer as timer
import face_recognition


def recognize(filename):
    """Does the actual recognizition of one image against all other images in the folder
    Args:
        filename (.jpg): Image to be chacked

    Returns:
        list: image, image chacked, face distance
    """
    sub_start = timer()
    directory = "/Users/remco/Desktop/Face Database"
    filelist = glob.glob(os.path.join(directory, '*.jpg'))
    filelist = sorted(filelist)
    image = face_recognition.load_image_file(filename)
    image_encoding = face_recognition.face_encodings(image)[0]
    res2 = []
    print("starting work on: " + str(filename))

    for filename2 in filelist:
        # Get the required information for the second image
        # First: get the image path; second: load the image; last: create the face encoding
        image2 = face_recognition.load_image_file(filename2)
        image2_encoding = face_recognition.face_encodings(image2)[0]
        # Calculate the euclidean distance between the two faces
        face_distances = face_recognition.face_distance([image_encoding], image2_encoding)

        # Not sure what the euclidean distance gives. Uncomment next line to reverse it)
        face_distances = 1 - face_distances
        output = [filename, filename2, str(face_distances[0]).rstrip('\n')]
        # Write the distance to a variable
        res2.append(output)
    sub_end = timer()
    sub_time = sub_end - sub_start
    print(f"work on: {str(filename)} took {sub_time}")
    return res2

if __name__ == "__main__":
    """
    Main proram
    """
    print("Starting program")
    start = timer()
    # Get the required information for the first image
    # First: get the image path; second: load the image; last: create the face encoding
    directory = "/Users/remco/Desktop/Face Database"
    filelist = glob.glob(os.path.join(directory, '*.jpg'))

    #Multi processing
    used_cores = mp.cpu_count() - 1
    print('number of used cores is: ' + str(used_cores))
    with mp.Pool(used_cores) as pool:
        results = pool.map(recognize, sorted(filelist))
    print(results)

    # write output to file
    file = open("/Users/remco/Desktop/RvS_Results.txt", "w")
    for result in results:
        file.write(str(result))
    file.close()
    end = timer()
    print(f'recognize time took: {end - start}')
    print("Program finished")